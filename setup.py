from setuptools import setup

setup (
        name="thonny-ssh",
        version="1.0",
        description="SSH backend for Thonny",
        long_description="""Thonny plug-in which adds an interpreter in a remote machine through SSH. More info about Thonny: https://thonny.org.""",
        url="https://bitbucket.org/Starrimus/thonny-ssh/",
        author="Lauri Leiten",
        author_email="leitenlauri@gmail.com",
        license="MIT",
        classifiers=[
        ],
        keywords="SSH Thonny",
        platforms=["Windows", "macOS", "Linux"],
        python_requires=">=3.6",
        install_requires=["ssh2-python"],
        packages=["thonnycontrib.ssh"],
)