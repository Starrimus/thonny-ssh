import platform
import re
import secrets
import threading
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as messagebox
from collections import namedtuple
from queue import Queue
from socket import socket
from time import sleep
from tkinter import Event
from tkinter.filedialog import askopenfilename
from typing import Optional, List, Type, Dict, Tuple
from urllib.request import Request, urlopen

from datetime import datetime
from ssh2.channel import Channel
from ssh2.exceptions import SFTPProtocolError, AuthenticationError, FileError, Timeout
from ssh2.session import Session
from ssh2.sftp import SFTP, LIBSSH2_FXF_WRITE, LIBSSH2_SFTP_S_IRWXU, LIBSSH2_FXF_CREAT, LIBSSH2_SFTP_S_IRWXG, \
    LIBSSH2_SFTP_S_IRWXO, LIBSSH2_FXF_READ, LIBSSH2_SFTP_S_IRUSR
from ssh2.sftp_handle import SFTPHandle

from thonny import get_runner
from thonny import get_workbench
from thonny.code import get_saved_current_script_filename
from thonny.common import ToplevelResponse, CommandToBackend, BackendEvent, ToplevelCommand, InlineCommand
from thonny.plugins.backend_config_page import BackendDetailsConfigPage
from thonny.running import BackendProxy, get_frontend_python
from thonny.ui_utils import create_string_var, show_dialog

THONNY_LATEST: str = "https://github.com/thonny/thonny/releases/latest"
THONNY_DOWNLOAD_URL: str = "https://github.com/thonny/thonny/releases/download/v<version>/<filename>"
THONNY_LINUX_64BIT_FILENAME: str = "thonny-<version>-x86_64.tar.gz"
THONNY_LINUX_32BIT_FILENAME: str = "thonny-<version>-i686.tar.gz"

MIN_PYTHON_VERSION = "3.6"
REMOTE_FOLDER: str = ".thonny-ssh"
SUPPORTED_LOGIN_METHODS: List[str] = ["password", "publickey"]
LOGIN_METHODS_DATA_STRUCT: Dict[str, List[Type[str]]] = {"password": [str, str], "publickey": [str, str, str]}

COMMAND_END_TAG: str = "<EndThonnyCommand>"
Command = namedtuple("command", "command_type command")


class SSHClient(threading.Thread):
    """
    Handles communication with SSH server and sends correctly formatted output to the backend
    """

    # Inits the client with necessary data
    def __init__(self, ip: str, port: str, interpreter: str, login_method: str, login_data: list,
                 backend_proxy: BackendProxy = None,
                 queue: Queue = None):
        """
        Initializes the SSH client

        :param ip: the IP of the server
        :param port: the port of the server
        :param login_method: the login method selected by the user
        :param login_data: the necessary login data for the method
        :param backend_proxy: the backend that program output should be sent to
        :param queue: the queue where the commands should be read from
        """
        threading.Thread.__init__(self)

        # Set random session ID
        self.__session_id = secrets.token_urlsafe(16)
        self.__interpreter = interpreter

        # Login data validation
        if login_method not in SUPPORTED_LOGIN_METHODS:
            raise ValueError("This type of login method isn't allowed")
        if type(login_data) != list or LOGIN_METHODS_DATA_STRUCT.get(login_method) != [type(x) for x in login_data]:
            raise ValueError("The login data structure isn't correct")

        # Get hosts
        self.__local_host: str = platform.system()
        self.__remote_host: Optional[str] = None

        # Backend proxy
        if queue is not None:
            self.__backend_proxy: Optional[SSHBackendProxy] = backend_proxy
            if self.__backend_proxy is None:
                raise ValueError("Backend not valid")

        # Queue for commands, stdout and stderr
        self.__command_queue: Queue = queue

        # Inits SSH and channel variables
        self.__ssh: Optional[SSHWrapper] = SSHWrapper()
        self.__sftp: Optional[SFTPWrapper] = None
        self.__general_channel: Optional[Channel] = None
        self.__python_channel: Optional[Channel] = None

        # Ip validation
        if not validate_ipv4(ip):
            raise ValueError("IP is not valid")

        # Port validation
        if not validate_port(str(port)):
            raise ValueError("Port is not valid")

        self.__ip = ip
        self.__port = int(port)
        self.__python_active = False

        self.__login_method = login_method
        self.__login_data = login_data

    # needed method for thread
    def run(self):
        """
        Runs the main part of the class - setup and listening for commands sent.
        """
        assert self.__backend_proxy is not None
        try:
            is_successful, next_action = self.setup()

            if not is_successful:
                if next_action == "restart":
                    sleep(1)
                    get_runner().restart_backend(clean=False)
                return

            while True:
                # Blocks the executing of the program until queue has a command
                data: Command = self.__command_queue.get()
                stdout = ""
                if data.command_type == "exec_python":
                    stdout = self.__exec_python_command(data.command)
                elif data.command_type == "exec_command":
                    if "run" in data.command:
                        stdout = self.__run_current_file()
                    else:
                        stdout = self.__exec_shell_command(data.command)
                self.__send_success_backend(stdout)
        finally:
            self.__close()

    def __close(self):
        """
        Closes the two channels and the SSH connection.
        """
        if self.__general_channel is not None:
            self.__general_channel.close()

        if self.__python_channel is not None:
            self.__general_channel.close()

        if self.__ssh is not None:
            self.__ssh.close()

    def __send_success_backend(self, stdout):
        """
        Helper function for sending success (stdout) messages to the backend

        :param stdout: message to be sent to the backend
        """
        if len(stdout) > 0:
            self.__backend_proxy.send_success_message(stdout)
        self.__backend_proxy.send_success_status()

    def setup(self) -> (bool, str):
        """
        Creates the connection to the remote machine, authenticates the user and sets up all the other necessary
        requisites for successful sending of messages to the remote machine.

        :return: tuple, consists of a bool and a str. The boolean shows whether the setup was successful and the string consists of instructions.
        """
        # Lets the user know that the client is connecting to the server
        self.__backend_proxy.send_success_message("Connecting to server...\n")

        # Connects to the server and handles errors (invalid login info / server down)
        status = self.__ssh.connect(self.__ip, self.__port)
        if not status:
            self.__backend_proxy.send_error_message(
                "Connection failed. Are you sure the server is up?\nPlease try again by restarting the backend\n")
            return False, "nothing"

        # Authenticates the user
        available_auth_methods = self.__ssh.get_possible_auth(self.__login_data[0])
        if self.__login_method not in available_auth_methods:
            raise ValueError("Incorrect login method")
        if self.__login_method == "password":
            status = self.__ssh.auth_password(self.__login_data)
        elif self.__login_method == "publickey":
            status = self.__ssh.auth_publickey(self.__login_data)
        else:
            raise ValueError("Incorrect login method")
        if not status:
            self.__backend_proxy.send_error_message(
                "Login failed\nPlease try again by restarting the backend\n")
            return False, "restart"

        # SFTP
        self.__sftp = SFTPWrapper(self.__ssh.get_sftp())

        # Creates the channels
        self.__general_channel = self.__ssh.create_channel()
        self.__python_channel = self.__ssh.create_channel()

        self.__ssh.write(self.__general_channel, COMMAND_END_TAG)
        self.__ssh.write(self.__general_channel, "\n")

        self.__ssh.write(self.__python_channel, COMMAND_END_TAG)
        self.__ssh.write(self.__python_channel, "\n")

        # Get the remote host
        data_general = self.__ssh.read_all(self.__general_channel, True)
        data_python = self.__ssh.read_all(self.__python_channel, True)

        sleep(0.5)

        self.__remote_host = self.__get_remote_os(data_general)

        # Lets the user know that the connection was made and lets them send commands
        self.__backend_proxy.send_success_message("Connection successful!\n")

        try:
            status, message = self.__setup_remote_python()
            self.__python_active = self.__is_python_running()
            if not status:
                raise ValueError("Installing Thonny failed.")
            if not self.__python_active:
                raise ValueError("Installing Thonny failed.")
            self.__backend_proxy.send_success_message(message)
        except ValueError as e:
            self.__backend_proxy.send_error_message("Setup failed. \nReason: " + str(e.args[0]))
            return False, "nothing"
        self.__backend_proxy.send_welcome_status()

        return True, "nothing"

    def __is_remote_python_available(self):
        """
        Checks whether a runnable remote Python exists in the remote machine
        :return: bool that shows whether a runnable remote python was found
        """
        python_execs = ["python3", "python", self.__user_directory + "/.thonny-ssh/backend/bin/python3"]
        for python_exec in python_execs:
            result = self.__exec_shell_command(python_exec + " --version")
            valid = is_python_valid(result, python_exec)
            if valid:
                return True, python_exec
        return False, ""

    def __setup_remote_python(self):
        """
        Setups the Thonny environment in the remote machine.
        :return: tuple of bool and str. The bool shows whether the setup was successful and string is the message to send to the backend.
        """

        sleep(1)

        # Gets the user directory from the server
        self.__user_directory = self.__exec_shell_command("echo ~" + self.__login_data[0])

        if self.__interpreter:
            result = self.__exec_shell_command(self.__interpreter + " --version")
            if is_python_valid(result, self.__interpreter):
                self.__python_exec = self.__interpreter
                self.__exec_python_command(self.__interpreter)
                if self.__is_python_running():
                    return True, "Setup complete\n"

        self.__backend_proxy.send_success_message("Setup of Thonny in the remote machine...\n")

        # Tries to get latest Thonny version
        thonny_version = ""
        thonny_version_success = True
        try:
            thonny_version = self.__get_latest_thonny_version()
        except Exception:
            thonny_version_success = False

        # Saves the filename to download from github if preexisting Python isn't found
        if "Linux" in self.__remote_host:
            if "64" in self.__remote_host:
                thonny_filename = THONNY_LINUX_64BIT_FILENAME.replace("<version>", thonny_version)
            else:
                thonny_filename = THONNY_LINUX_32BIT_FILENAME.replace("<version>", thonny_version)
        else:
            raise ValueError("Remote operating system " + self.__remote_host + " isn't supported.")

        # Makes sure the session is in the user directory
        self.__exec_shell_command("cd " + self.__user_directory)

        # Checks whether remote python is available and if it is runs it in the python channel.
        status, python_exec = self.__is_remote_python_available()
        if status:
            self.__existing_python = True
            self.__python_exec = python_exec
            self.__exec_python_command(python_exec)
            return True, "Preexisting compatible Python found.\nSetup complete\n"

        # If Thonny version fetch failed the setup process cannot go forward
        if not thonny_version_success:
            return False, "failed"

        # Creates necessary folders for the download
        self.__exec_shell_command("mkdir .thonny-ssh")
        self.__exec_shell_command("mkdir .thonny-ssh/backend")

        # Move the shell to the necessary folder
        self.__exec_shell_command("cd .thonny-ssh/backend")

        # Deletes the downloadable file if it already exists
        self.__sftp.delete_file(self.__user_directory + ".thonny-ssh/backend/" + thonny_filename)

        # Downloads the backend to the remote machine
        thonny_download_url = THONNY_DOWNLOAD_URL.replace("<filename>", thonny_filename).replace("<version>",
                                                                                                 thonny_version)
        self.__exec_shell_command("wget " + thonny_download_url)
        self.__exec_shell_command("tar --strip-components=1 -zxf " + thonny_filename)

        # Deletes the .tar.gz file
        self.__sftp.delete_file(self.__user_directory + ".thonny-ssh/backend/" + thonny_filename)

        # Moves the working directory back to the user dir
        self.__exec_python_command("cd " + self.__user_directory)

        # Checks whether the setup was successful and tries to run python if it was.
        status, python_exec = self.__is_remote_python_available()
        if status:
            self.__existing_python = True
            self.__python_exec = python_exec
            self.__exec_python_command(python_exec)
            return True, "Setup complete\n"

        return False, "failed"

    def __is_python_running(self):
        """
        Checks whether python is running in the remote machine
        :return: bool
        """
        self.__exec_python_command("import platform")
        test_command = self.__exec_python_command("platform.system()")
        if self.__remote_host.split("_")[0] not in test_command:
            return False
        return True

    @staticmethod
    def __get_latest_thonny_version() -> str:
        """
        Gets the lates thonny version from github
        :return: latest version
        """
        request = Request(THONNY_LATEST)
        result = urlopen(request)
        return result.geturl().split("/").pop()[1:]

    def __get_remote_os(self, msg):
        """
        Finds the operating system of the remote machine

        :param msg: welcome message
        :return: the operating system of the remote machine
        """
        # Check whether the welcome message contains the remote OS
        if "Linux" in str(msg):
            if "x86_64" in msg:
                return "Linux_64"
            else:
                return "Linux_32"
        elif "Windows" in str(msg):
            return "Windows"
        elif "OS X" in str(msg):
            return "OS X"

        # Run a Windows-specific command to see if it is a Windows machine
        message = self.__exec_shell_command("ver")
        if "Windows" in message:
            return "Windows"

        # Run a Linux-specific command to see if it is a Linux machine
        message = self.__exec_shell_command("cat /proc/version")
        if "Linux" in message:
            if "x86_64" in message:
                return "Linux_64"
            else:
                return "Linux_32"

        # Run a Mac-specific command to see if it is a Mac machine
        message = self.__exec_shell_command("system_profiler SPSoftwareDataType")
        if "Mac OS X" in message:
            return "OS X"

        return "Unknown"

    def __exec_shell_command(self, command: str) -> (str, str):
        """
        Executes command in the system shell
        :param command: command to execute
        :return: formatted output
        """
        stdout = self.__ssh.exec(channel=self.__general_channel, end_tag=self.__get_linesep(), command=command)
        stdout = self.format_linux_shell_output(stdout)
        return stdout

    def __exec_python_command(self, command: str) -> (str, str):
        """
        Executes command in the Python shell
        :param command: command to execute
        :return: formatted output
        """
        if self.__python_active:
            stdout = self.__ssh.exec(channel=self.__python_channel, end_tag=self.__get_linesep(), command=command,
                                     python_command=True)
            stdout = self.format_python_shell_output(stdout, command)
        else:
            stdout = self.__ssh.exec(channel=self.__python_channel, end_tag=self.__get_linesep(), command=command)
            stdout = self.format_linux_shell_output(stdout)
        return stdout

    def __get_linesep(self, requested_os_sep=None) -> str:
        """
        Gets the correct line separator for the remote host
        :param requested_os_sep: optional, if set tries getting line separator for that OS
        :return: line separator
        """
        if requested_os_sep is None:
            if self.__remote_host is None:
                return "\n"
            requested_os_sep = self.__remote_host

        if requested_os_sep == "Windows":
            return "\n"
        else:
            return "\n"

    def format_python_shell_output(self, msg: str, cmd: str):
        """
        Formats output from python shell (removes preceding and trailing lines)
        :param msg: msg to format
        :param cmd: command send to the shell
        :return:
        """
        if msg == "" or msg is None:
            return ""

        replace_char = ''
        if self.__remote_host == "Windows":
            replace_char = "\n"
        msg_lines = list(filter(bool, replace_shell_codes(msg, replace_char).splitlines()))

        if len(msg_lines) < 3:
            return ""

        lines_command = cmd.splitlines()
        for i in range(len(lines_command)):
            msg_lines.pop(0)

        msg_lines.pop()
        msg_lines.pop()

        return "\n".join(msg_lines)

    def format_linux_file_output(self, msg: str):
        """
        Formats output from file running (removes preceding and trailing lines)
        :param msg: msg to format
        :return:
        """
        if msg == "" or msg is None:
            return ""

        # Removes color codes and everything after end tags
        msg_lines = replace_shell_codes(msg).splitlines()

        if len(msg_lines) < 6:
            return "\n".join(msg_lines)

        for i in range(5):
            msg_lines.pop(0)

        # The prompt for new command
        msg_lines.pop()

        return "\n".join(msg_lines)

    def format_linux_shell_output(self, msg: str):
        """
        Formats output from linux shell (removes preceding and trailing lines)
        :param msg: msg to format
        :return:
        """
        if msg == "" or msg is None:
            return ""

        # Removes color codes and everything after end tags
        msg_lines = replace_shell_codes(msg).splitlines()

        if len(msg_lines) < 2:
            return "\n".join(msg_lines)

        # Removes first line (the command ran itself)
        msg_lines.pop(0)

        # The prompt for new command
        msg_lines.pop()

        return "\n".join(msg_lines)

    def __run_current_file(self):
        """
        Runs the current open file in Thonny in system shell
        :return: output value
        """
        channel = self.__general_channel

        self.__exec_shell_command("cd " + self.__user_directory)
        self.__sftp.create_folder(REMOTE_FOLDER)

        self.__sftp.upload_file('/'.join([REMOTE_FOLDER, self.__session_id + ".py"]))
        commands_to_write = "touch " + self.__user_directory + "/.thonny-ssh/" + self.__session_id + "_file && " + self.__python_exec + " .thonny-ssh/" + self.__session_id + ".py ; rm " + self.__user_directory + "/.thonny-ssh/" + self.__session_id + "_file"
        self.__ssh.write(channel, commands_to_write)
        self.__ssh.write(channel, "\n")
        while True:
            try:
                sleep(0.1)
                file: SFTPHandle = self.__sftp.open_file(
                    self.__user_directory + "/.thonny-ssh/" + self.__session_id + "_file",
                    LIBSSH2_FXF_READ,
                    LIBSSH2_SFTP_S_IRUSR)
                file.close()
            except SFTPProtocolError:
                break
        return self.format_linux_file_output(self.__ssh.read_all(channel, False, False))


class SSHBackendProxy(BackendProxy):
    """
    Handles getting SSH login info from the user, instantiates and communicates with a SSH client.
    """

    def __init__(self, clean):
        """
        Initializes the SSH backend process.
        """
        super().__init__(clean)

        self.__message_queue = Queue()
        self.__command_queue = Queue()

        self.__ip: str = get_workbench().get_option("ssh.ip")
        self.__port: str = get_workbench().get_option("ssh.port")
        self.__interpreter: str = get_workbench().get_option("ssh.interpreter")

        self.__ssh: Optional[SSHClient] = None

        self.__login_prompt = SSHLoginInfoPrompt(self.__ip, self.__port)
        self.__login_prompt.bind("<<LoginInfoPromptSubmit>>", self.__login_prompt_submit_callback)
        self.__login_prompt.protocol("WM_DELETE_WINDOW", self.__login_prompt_destroy_callback)

        show_dialog(self.__login_prompt)

    # Below are the login prompt callbacks

    def __login_prompt_submit_callback(self, event: Event) -> None:
        """
        The callback method of tkinter event <<LoginInfoPromptSubmit>>. The event is triggered after the user submits
        the login prompt. This method instantiates a SSH client for the backend proxy class and destroys the login
        prompt window.

        :param event: the event that called this method
        """
        try:
            self.__ssh = SSHClient(self.__ip, self.__port, self.__interpreter,
                                   self.__login_prompt.login_method.get().lower(),
                                   self.__login_prompt.login_data, self, self.__command_queue)
            self.__ssh.start()
        except ValueError:
            self.send_error_message("Something went wrong\nPlease try again by restarting the backend\n")

        self.__login_prompt.destroy()

    def __login_prompt_destroy_callback(self) -> None:
        """
        The callback for protocol WM_DELETE_WINDOW. This method finishes destroying the login prompt window. If no
        input was detected from login prompt then an error message is shown in the user's shell.
        """
        if self.__ssh is None:
            self.send_error_message("The login prompt was closed without any input\nYou can open the prompt again by "
                                    "restarting the backend\n")
        elif self.__login_prompt.connect_thread.is_alive():
            self.send_error_message("Something went wrong\nYou can open the prompt again by "
                                    "restarting the backend\n")
        self.__login_prompt.destroy()

    # Below are the methods for sending shell messages

    def __send_message(self, event_type: str, stream_name: str, data: str):
        if not data:
            data = "No output received!\n"
        self.__message_queue.put(BackendEvent(event_type=event_type,
                                              stream_name=stream_name,
                                              data=data))

    def send_success_message(self, msg: str) -> None:
        self.__send_message("ProgramOutput", "stdout", msg)

    def send_error_message(self, msg: str) -> None:
        self.__send_message("ProgramOutput", "stderr", msg)

    # Below are the methods for sending status messages

    def __send_status(self, item: str, text: str):
        response = ToplevelResponse()
        response.__setitem__(item, text)
        self.__message_queue.put(response, True)

    def send_welcome_status(self):
        self.__send_status("welcome_text", "")

    def send_empty_status(self):
        self.__send_status("nothing", "")

    def send_success_status(self):
        self.__send_status("success", "")

    def send_error_status(self):
        self.__send_status("error", "")

    # Below are the methods from the parent class

    def send_command(self, cmd: CommandToBackend):
        try:
            if isinstance(cmd, ToplevelCommand):
                command_type, cmd = parse_command(cmd)
                if command_type == "nothing":
                    # Sends empty status, that means - nothing should be done with the command
                    self.send_empty_status()
                elif command_type == "error":
                    # If the input was erroneous e.g magic command that's not allowed
                    self.send_error_message(cmd)
                    self.send_error_status()
                else:
                    # Puts the command and it's type in a queue. This queue is shared with the SSH client.
                    self.__command_queue.put(Command(command_type, cmd))
            elif isinstance(cmd, InlineCommand):
                pass
            else:
                raise ValueError
        except ValueError:
            self.send_error_message("Something went wrong with running the command!")
            self.send_error_status()

    def fetch_next_message(self):
        if self.__message_queue.qsize() > 0:
            return self.__message_queue.get_nowait()
        return None

    def send_program_input(self, data: str) -> None:
        pass

    def run_script_in_terminal(self, script_path, interactive, keep_open):
        pass


"""
Helper classes for SSH and SFTP connections
"""


class SSHWrapper:
    """
    Lightweight wrapper for SSH related operations
    """

    def __init__(self):
        """
        Initializes the wrapper
        """
        self.__socket: Optional[socket] = None
        self.__session: Optional[Session] = None

    def close(self):
        """
        Closes the SSH session and socket if possible.
        """
        if self.__session is not None:
            self.__session.disconnect()

        if self.__socket is not None:
            self.__socket.close()

    def connect(self, ip: str, port: str) -> bool:
        """
        Connects to the server through SSH
        :param ip: IP of the remote server
        :param port: port of the remote SSH server
        :return: success status
        """
        try:
            if not validate_port(port):
                return False
            port_int = int(port)

            # Create socket for communication
            sock: socket = socket()
            sock.connect((ip, port_int))

            # Create a session that uses the previously created socket
            session: Session = Session()
            session.set_blocking(True)
            session.handshake(sock)

            self.__socket = sock
            self.__session = session

            return True
        except Exception as e:
            return False

    def get_possible_auth(self, username: str) -> list:
        """
        Gets possible authentication methods
        :param username: gets authentication methods for this user
        :return: authentication methods
        """
        if not self.__socket or not self.__session:
            raise ValueError("Connection has to be created before ")

        return self.__session.userauth_list(username)

    def auth_password(self, login_data: list) -> bool:
        """
        Authenticates user using password-method
        :param login_data: password method data
        :return: success status
        """
        if not self.__socket or not self.__session:
            raise ValueError("Connection has to be created before ")

        username = login_data[0]
        password = login_data[1]

        try:
            self.__session.userauth_password(username, password)
            return True
        except AuthenticationError:
            return False

    def auth_publickey(self, login_data: list) -> bool:
        """
        Authenticates user using publickey-method
        :param login_data: publickey method data
        :return: success status
        """
        if not self.__socket or not self.__session:
            raise ValueError("Connection has to be created before ")

        username = login_data[0]
        private_key = login_data[1]
        private_key_passcode = login_data[2]

        try:
            self.__session.userauth_publickey_fromfile(username, private_key, private_key_passcode)
            return True
        except AuthenticationError:
            return False

    def get_sftp(self) -> SFTP:
        """
        Inits the SFTP session
        :return: SFTP session
        """
        if not self.__socket or not self.__session:
            raise ValueError("Connection has to be created before ")

        return self.__session.sftp_init()

    def create_channel(self, pty=True) -> Channel:
        """
        Creates a SSH channel
        :param pty: if pseudo-terminal should be created
        :return: created channel
        """
        channel: Channel = self.__session.open_session()
        if pty:
            channel.pty()
        channel.shell()
        return channel

    def exec(self, channel: Channel, end_tag: str, command: str, python_command: bool = False) -> (str, str):
        """
        Executes a command to given channel
        :param channel: channel to execute the command in
        :param end_tag: line separator to write after command
        :param command: command to execute
        :param python_command: if the command executed is a python command
        :return: command output
        """
        if not self.__session.userauth_authenticated():
            return ""

        self.write(channel, command)
        channel.write(end_tag)
        channel.flush()
        content = self.read_all(channel, False, python_command)

        self.write(channel, COMMAND_END_TAG)
        channel.write(end_tag)
        channel.flush()
        content += self.read_all(channel, True)

        return content

    def write(self, channel: Channel, command: str) -> None:
        """
        Writes a command to given channel
        :param channel: channel to write to
        :param command: command to write
        :return:
        """
        if not self.__session.userauth_authenticated():
            return

        size = channel.write(command)
        while size[1] != len(command):
            channel.write(command[size[1]:])

    def read_all(self, channel: Channel, end_tag=False, python_command=False) -> str:
        """
        Reads all command output from the given channel
        :param channel: channel to read from
        :param end_tag: whether the method should search for end command
        :param python_command: whether the command executed was python command
        :return: command output
        """
        if not self.__session.userauth_authenticated():
            return ""

        size, data_all = channel.read(2048)
        try:
            self.__session.set_timeout(1500)
            while (data_all.rfind(COMMAND_END_TAG.encode("utf-8")) == -1 and end_tag) or (
                    (size > 0 or len(data_all) == 0) and not end_tag) or \
                    (data_all.rfind(">>>".encode("UTF-8")) == -1 and python_command):
                try:
                    size, data = channel.read(2048)
                    data_all += data
                except Timeout:
                    size = 0
        finally:
            self.__session.set_timeout(0)

        return data_all.decode("utf-8")


class SFTPWrapper:
    """
    Lightweight wrapper for SFTP related operations
    """

    def __init__(self, sftp: SFTP):
        """
        Initializes the class
        :param sftp: SFTP session
        """
        self.__sftp = sftp

    def create_folder(self, path: str) -> None:
        """
        Creates a folder if one doesnt exist with permissions drwxrwxrwx
        :param path: folder to create
        """
        try:
            self.__sftp.mkdir(path, LIBSSH2_SFTP_S_IRWXU | LIBSSH2_SFTP_S_IRWXG | LIBSSH2_SFTP_S_IRWXO)
        except SFTPProtocolError as e:
            pass

    def open_file(self, path: str, flags, mode):
        """
        Opens a file
        :param path: file to open
        :param flags: flags to open the file with. Flags include read / write / create for example
        :param mode: file attributes
        :return: file handle
        """
        return self.__sftp.open(path, flags, mode)

    def delete_file(self, filename: str) -> None:
        """
        Deletes a file if one exists with given name
        :param filename: file to delete
        """
        try:
            self.__sftp.unlink(filename)
        except SFTPProtocolError:
            pass

    def upload_file(self, remote_path, local_path=None):
        """
        Uploads a file in local file system to remote server.
        If the file with the same name already exists, overwrites the previous file.
        :param local_path: path of file to upload
        :param remote_path: path of the file in the remote server
        """
        if local_path is None:
            local_path = get_saved_current_script_filename()
        self.delete_file(remote_path)
        with open(local_path, 'rb') as local_handle:
            remote_handle: SFTPHandle = self.__sftp.open(remote_path, LIBSSH2_FXF_CREAT | LIBSSH2_FXF_WRITE,
                                                         LIBSSH2_SFTP_S_IRWXU)
            for data in local_handle:
                remote_handle.write(data)


"""
User interface
"""


class SSHOneTimePrompt(tk.Toplevel):
    """
    User interface that creates a connection separate from backend. Used for uploading the current file.
    """

    def __init__(self, master=None):
        """
        Initializes the interface
        """
        super().__init__(master)
        self.resizable = False
        self.title = "Upload script"

        self.ip_validation_callback = self.master.register(validate_ipv4)
        self.port_validation_callback = self.master.register(validate_port)

        self.host = tk.StringVar()
        self.port = tk.StringVar()

        self.file_name = get_saved_current_script_filename()

        self.__create_dialog()

    def __submit(self):
        """
        Validates input and starts the connecting process to the server.
        """
        ip_status = validate_ipv4(self.host.get())
        port_status = validate_port(self.port.get())
        if not ip_status or not port_status:
            if not ip_status:
                self.ip_entry.config({"background": "light pink"})
            else:
                self.ip_entry.config({"background": "white"})
            if not port_status:
                self.port_entry.config({"background": "light pink"})
            else:
                self.port_entry.config({"background": "white"})
            return

        # Destroys server info prompt
        self.destroy()

        # Creates the second prompt
        self.__second_prompt = SSHLoginInfoPrompt(self.host.get(), self.port.get(), "Upload file...")

        # Binds submit event from the user data prompt
        self.__second_prompt.bind("<<LoginInfoPromptSubmit>>", self.__login_prompt_submit_callback)

        # Centers the new prompt
        show_dialog(self.__second_prompt)

    # Below are the login prompt callbacks

    def __login_prompt_submit_callback(self, event: Event) -> None:
        """
        The callback method of tkinter event <<LoginInfoPromptSubmit>>. The event is triggered after the user submits
        the login prompt. This method instantiates a SSH client for the backend proxy class and destroys the login
        prompt window.

        :param event: the event that called this method
        """
        self.__second_prompt.destroy()
        try:
            ssh = SSHWrapper()
            if not ssh.connect(self.host.get(), self.port.get()):
                messagebox.showerror("Upload", "Connection failed!")
                return

            if self.__second_prompt.login_method.get() == "password":
                ssh.auth_password(self.__second_prompt.login_data)
            elif self.__second_prompt.login_method.get() == "publickey":
                ssh.auth_publickey(self.__second_prompt.login_data)
            else:
                raise ValueError("Incorrect login method")

            self.__sftp = SFTPWrapper(ssh.get_sftp())
            self.__sftp.upload_file(str(datetime.now()) + "-" + secrets.token_urlsafe(16) + "_thonny_file.py")
            messagebox.showinfo("Upload", "File upload was successful!")
        except Exception as e:
            messagebox.showerror("Upload", "File upload failed!")

    def __create_dialog(self):
        """
        Creates the dialog that asks user for the remote server information.
        """

        tk.Label(self, text="Remote server IP: ").grid(row=0)  # IP label
        self.ip_entry = tk.Entry(self, textvariable=self.host, validate="focusout",
                                 validatecommand=(self.ip_validation_callback, '%P'))
        self.ip_entry.grid(row=1, pady=(5, 5), padx=(5, 5))

        tk.Label(self, text="Remote server port: ").grid(row=2)  # Port label
        self.port_entry = tk.Entry(self, textvariable=self.port, validate="focusout",
                                   validatecommand=(self.port_validation_callback, '%P'))
        self.port_entry.grid(row=3, pady=(5, 5), padx=(5, 5))

        self.run_button = ttk.Button(self, text="Connect", command=self.__submit)
        self.run_button.grid(row=9, pady=(5, 5), padx=(5, 5), sticky="nsew")


class SSHLoginInfoPrompt(tk.Toplevel):
    """
    The login prompt asks the user for it's login information for connecting to the remote machine through SSH.
    """

    def __init__(self, ip: str, port: str, button_text: str = "Login", master=None):
        """
        Initializes the class and creates the login prompt window.
        """
        super().__init__(master)

        self.__ip = ip
        self.__port = port
        self.__button_text = button_text

        button_invalid = ttk.Style()
        button_invalid.configure("Invalid.TButton", background="light pink")

        # Frame that holds the username field and the "Show login methods", "Connect" buttons
        self.__initial_frame = tk.Frame(self)
        self.__initial_frame.pack(side="left", fill="both", padx=(5, 5), pady=(5, 5))

        # Username label
        self.__username_label = tk.Label(self.__initial_frame, text="Username")
        self.__username_label.grid(row=0, column=0, columnspan=2)

        # Username entry
        self.username = tk.StringVar()
        self.__username_entry = tk.Entry(self.__initial_frame, textvariable=self.username)
        self.__username_entry.grid(row=1, column=0, columnspan=2, padx=(5, 5))

        # Button "Connect"
        self.submit = ttk.Button(self.__initial_frame, text=self.__button_text, command=self.submit, state="disabled")
        self.submit.grid(row=3, columnspan=2, pady=(20, 5), padx=(5, 5), sticky="nsew")

        # Status area
        self.status = tk.Label(self.__initial_frame, text="Status: connecting...")
        self.status.grid(row=4, columnspan=2, padx=(5, 5), sticky="nsew")

        self.login_method = tk.StringVar()
        self.__details_nb: Optional[ttk.Notebook] = None

        self.connect_thread = threading.Thread(name="Connect thread", target=self.__create_connection)
        self.connect_thread.start()

    def submit(self, _event=None) -> None:
        """
        Handles the submission of login information. After validation generates an event called <<LoginInfoPromptSubmit>>.
        """
        valid = True
        username_not_empty = not_empty(self.username.get())
        if not username_not_empty:
            self.__username_entry.config({"background": "light pink"})
            valid = False
        else:
            self.__username_entry.config({"background": "white"})

        self.login_method.set(self.__details_nb.tab(self.__details_nb.select(), "text").lower())
        if self.login_method.get() == "password":
            password_not_empty = not_empty(self.password.get())
            if not password_not_empty:
                self.__password_entry.config({"background": "light pink"})
                valid = False
            else:
                self.__password_entry.config({"background": "white"})
        elif self.login_method.get() == "publickey":
            key_not_empty = not_empty(self.filename.get())
            if not key_not_empty:
                self.__filename_button.config({"style": "Invalid.TButton"})
                valid = False
            else:
                self.__filename_button.config({"style": "TButton"})
        else:
            return

        if not valid:
            return

        self.event_generate("<<LoginInfoPromptSubmit>>")

    def __create_connection(self):
        """
        Creates a session to the remote machine. Meant to be used in a separate thread
        """
        self.__ssh = SSHWrapper()
        status = self.__ssh.connect(self.__ip, self.__port)
        if not status:
            if not self.winfo_exists():
                return
            self.__connect_fail()
            return
        if not self.winfo_exists():
            return
        if self.submit:
            self.submit.config(state="normal")
            self.__connect_success()

    def __connect_fail(self):
        """
        Shows connection fail status to the user
        """
        if self.status is not None:
            self.status.config(text="Status: connection failed")
            self.status.config(fg="red")

    def __connect_success(self):
        """
        Gets available login methods from the remote server and displays them with necessary information to the user.
        """
        self.status.config(text="Status: getting login methods")
        if self.__details_nb is None:
            self.__details_nb = ttk.Notebook(self)
        for child in self.__details_nb.winfo_children():
            child.destroy()
        for method in self.__get_available_login_methods():
            tab = tk.Frame(self.__details_nb)
            if method == "password":
                label = tk.Label(tab, text="Password")
                label.grid(row=1, column=0, columnspan=2)
                self.password = tk.StringVar()
                self.__password_entry = tk.Entry(tab, textvariable=self.password, show="*")
                self.__password_entry.grid(row=2, column=0, columnspan=2, padx=(5, 5))
            elif method == "publickey":
                label = tk.Label(tab, text="Private key")
                label.grid(row=1, column=0, columnspan=2)
                self.filename = tk.StringVar()
                self.__filename_button = ttk.Button(tab, text="Select file...", command=self.__browse)
                self.__filename_button.grid(row=2, column=0, columnspan=2, padx=(5, 5))

                label = tk.Label(tab, text="Private key passcode")
                label.grid(row=3, column=0, columnspan=2)
                self.private_key_password = tk.StringVar()
                self.__private_key_password_entry = tk.Entry(tab, textvariable=self.private_key_password, show="*")
                self.__private_key_password_entry.grid(row=4, column=0, columnspan=2, padx=(5, 5))
            self.__details_nb.add(tab, text=method.capitalize())
        self.status.destroy()
        self.__details_nb.pack(side="left", fill="both", padx=(5, 5), pady=(5, 5))

    def __get_available_login_methods(self) -> list:
        """
        Gets available login methods from the remote machine and filters out the nonsupported methods
        :return: Available and supported login methods in a list
        """
        available_methods = self.__ssh.get_possible_auth(self.username.get())
        if available_methods is None:
            return []
        return [method for method in available_methods if method in SUPPORTED_LOGIN_METHODS]

    def __browse(self):
        self.filename.set(askopenfilename())

    @property
    def login_data(self) -> list:
        if self.login_method.get() == "password":
            return [self.username.get(), self.password.get()]
        elif self.login_method.get() == "publickey":
            return [self.username.get(), self.filename.get(), self.private_key_password.get()]
        return []


class SSHBackendConfig(BackendDetailsConfigPage):
    """
    Config page for SSH backend.
    """

    def __init__(self, master=None):
        """
        Initializes the class and creates the elements of the config page.
        """
        super().__init__(master)

        # Init configuration fields
        get_workbench().set_default("ssh.ip", "127.0.0.1")
        get_workbench().set_default("ssh.port", "22")
        get_workbench().set_default("ssh.interpreter", "")

        # Init variables for IP and PORT
        self.ip = create_string_var(get_workbench().get_option("ssh.ip", "127.0.0.1"))
        self.port = create_string_var(get_workbench().get_option("ssh.port", "22"))
        self.remote_interpreter = create_string_var(get_workbench().get_option("ssh.interpreter", ""))

        # Validation callbacks
        self._ip_validation_callback = self.master.register(validate_ipv4)
        self._port_validation_callback = self.master.register(validate_port)
        self._interpreter_validation_callback = self.master.register(validate_path)

        # Create IP label and entry
        self.ip_label = tk.Label(self, text="Remote server IP: ")
        self.ip_label.grid(row=0, column=0, columnspan=2, sticky=tk.W)
        self.ip_entry = tk.Entry(self, textvariable=self.ip, validate="focusout",
                                 validatecommand=(self._ip_validation_callback, "%P"))
        self.ip_entry.grid(row=1, column=0, columnspan=2, sticky=tk.NSEW)

        # Create PORT label and entry
        self.port_label = tk.Label(self, text="Remote server port:")
        self.port_label.grid(row=2, column=0, columnspan=2, sticky=tk.W)

        self.port_entry = tk.Entry(self, textvariable=self.port, validate="focusout",
                                   validatecommand=(self._port_validation_callback, "%P"))
        self.port_entry.grid(row=3, column=0, columnspan=2, sticky=tk.NSEW)

        # Create INTEPRETER label and entry
        self.interpreter_label = tk.Label(self, text="Remote server Python PATH (optional):")
        self.interpreter_label.grid(row=4, column=0, columnspan=2, sticky=tk.W)

        self.interpreter_entry = tk.Entry(self, textvariable=self.remote_interpreter, validate="focusout",
                                          validatecommand=(self._interpreter_validation_callback, "%P"))
        self.interpreter_entry.grid(row=5, column=0, columnspan=2, sticky=tk.NSEW)

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

    def should_restart(self):
        return self.ip.modified or self.port.modified or self.remote_interpreter.modified

    def apply(self):
        if not self.should_restart():
            return

        if validate_ipv4(self.ip.get()):
            get_workbench().set_option("ssh.ip", self.ip.get())
        if validate_port(self.port.get()):
            get_workbench().set_option("ssh.port", self.port.get())
        if validate_path(self.remote_interpreter.get()):
            get_workbench().set_option("ssh.interpreter", self.remote_interpreter.get())


"""
Helper functions
"""


def is_command_allowed(cmd: str) -> bool:
    """
    Checks whether the requested Python command is allowed
    :param cmd: the command
    :return: if the command is allowed to be ran
    """
    not_allowed_commands = ["quit()"]

    for not_allowed_command in not_allowed_commands:
        if not_allowed_command in cmd:
            return False
    return True


def is_python_valid(result, python_exec):
    try:
        result = re.search('(Python ([\d].)*(\d))', result).group(1)
    except AttributeError:
        return False

    result_split = result.split(" ")
    if result_split[0] != "Python":
        return False
    if not re.match("([\d].)*(\d)", result_split[1]):
        return False
    if len(result_split) != 2:
        return False

    cmp = compare_versions(MIN_PYTHON_VERSION, result_split[1]) <= 0
    if cmp:
        return True

    return False


def replace_shell_codes(msg: str, replace_char='') -> str:
    # Source: https://stackoverflow.com/questions/30425105/filter-special-chars-such-as-color-codes-from-shell-output
    return re.sub(r'\x1b(\[.*?[@-~]|\].*?(\x07|\x1b\\))', replace_char, msg)


def compare_versions(version1, version2):
    # Source: https://stackoverflow.com/questions/1714027/version-number-comparison-in-python
    # By gnud
    def normalize(v):
        return [int(x) for x in re.sub(r'(\.0+)*$', '', v).split(".")]

    normalized_ver1 = normalize(version1)
    normalized_ver2 = normalize(version2)
    return (normalized_ver1 > normalized_ver2) - (normalized_ver1 < normalized_ver2)


def parse_command(cmd: ToplevelCommand) -> Tuple[str, str]:
    """
    Finds the command type and the command from a ToplevelCommand. Used for sending correct information to the SSH
    client.

    :param cmd: command to analyze
    :return: a tuple consisting of command type and the command itself.
    """
    name: str = cmd.get("name")
    if "cd" in name.lower():
        return "nothing", ""
    elif "run" in name.lower():
        return "exec_command", "run"
    elif name.lower() == "ls" or name.lower() == "ls -a" or name.lower() == "ls -a -l":
        return "exec_command", build_command(name, cmd.get("args"))
    elif name == "execute_source":
        command_stripped = cmd.get("source").strip("\n")
        if len(command_stripped) == 0:
            return "nothing", ""
        if not is_command_allowed(command_stripped):
            return "error", "Command not allowed!"
        return "exec_python", cmd.get("source")
    else:
        return "error", "No such command exists!"


def build_command(cmd: str, args: list) -> str:
    """
    Concatenates the command and it's arguments.
    """
    return " ".join([cmd] + args)


def validate_ipv4(ip: str) -> bool:
    """
    Validates an IPv4 address. A valid IPv4 must consist of 4 integer components, every component must be in range of
    0 and 256; e.g 127.0.0.1, 192.168.56.103;
    """

    def validate_ip_component(ip_part: int) -> bool:
        """
        Validates a component of ip. Must be in range of 0 and 256.
        """
        return 0 <= ip_part <= 256

    if ip is None:
        return False

    ip_components = ip.split(".")
    if len(ip_components) != 4:
        return False

    try:
        for i in range(4):
            if not validate_ip_component(int(ip_components[i])):
                raise ValueError()
    except ValueError:
        return False
    except IndexError:
        return False

    return True


def validate_port(port: str) -> bool:
    """
    Validates a port number. Valid port is an integer and is in the range of 0 and 65535.
    """
    if port is None:
        return False

    try:
        port: int = int(port)
        if not (0 < port <= 65535):
            raise ValueError()
    except ValueError:
        return False
    return True


def validate_path(path: str) -> bool:
    """
    Validates a path. Valid UNIX path starts with /
    """
    if path is None:
        return False

    return path.startswith("/")


def not_empty(data: any) -> bool:
    """
    Validates that the length of data is greater than 0
    """
    if data is None:
        return False
    return len(data) > 0


# Test info: IP: 192.168.56.103
# Port: 22
# username: test
# password: Test123
def load_plugin():
    def upload_current_file():
        ssh = SSHOneTimePrompt()
        show_dialog(ssh)

    get_workbench().add_backend("SSH", SSHBackendProxy, "Python in a remote machine (through SSH)", SSHBackendConfig)
    get_workbench().add_command(command_id="Upload script to a remote machine",
                                menu_name="SSH",
                                command_label="Upload current file to a remote machine",
                                handler=upload_current_file)
